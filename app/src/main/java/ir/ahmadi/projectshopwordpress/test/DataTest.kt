package ir.ahmadi.projectshopwordpress.test

import ir.ahmadi.projectshopwordpress.dataClass.DataComments
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct

class DataTest {

    companion object {

        fun getDataComments(): List<DataComments> {

            val array = arrayListOf<DataComments>()

            for (item in 1..8) {

                array.add(
                    DataComments(
                        item,
                        "علیرضا احمدی",
                        "قیمت محصول خیلی مناسب و کیفیت آن از قیمتش بسیار بهتر میباشد. محصولی مقرون به صرفه بوده و خرید آن به شدت توصیه میشود."
                    )
                )

            }

            return array

        }

    }

}