package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.adapter.RecyclerItemArchiveAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.etc.Utility
import kotlinx.android.synthetic.main.activity_archive.view.*
import org.jetbrains.anko.toast

@SuppressLint("ViewConstructor")
class ViewArchiveActivity(contextInstance: Context, private val utility: Utility) :
    FrameLayout(contextInstance) {

    private val txtTitle: AppCompatTextView
    private val imgBack: AppCompatImageView
    private val recycler: RecyclerView

    init {

        val mainView = inflate(context, R.layout.activity_archive, this)

        txtTitle = mainView.txt_title_archive_activity
        imgBack = mainView.img_back_archive_activity
        recycler = mainView.recyclerView_archive_activity

        recycler.layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)

    }

    fun setDataInRecyclers(data: List<DataProduct>) {

        recycler.adapter = RecyclerItemArchiveAdapter(context, data)
        recycler.visibility = View.VISIBLE
        progressBar_archive.visibility = View.INVISIBLE

    }

    fun setTitleText(title: String) {

        txtTitle.text = title

    }

    fun showToast(text: String) {
        context.toast(text)
    }

    fun onClickHandler() {

        imgBack.setOnClickListener { utility.onFinished() }

    }

}