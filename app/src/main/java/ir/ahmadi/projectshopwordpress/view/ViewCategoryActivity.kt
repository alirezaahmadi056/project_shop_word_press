package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.view.customView.ProductView
import kotlinx.android.synthetic.main.activity_category.view.*
import org.jetbrains.anko.toast

@SuppressLint("ViewConstructor")
class ViewCategoryActivity(
    contextInstance: Context,
    private val utility: Utility
) :
    FrameLayout(contextInstance) {

    private val root: RelativeLayout
    private val txtTitle: AppCompatTextView
    private val imgBack: AppCompatImageView
    private val newProduct: ProductView
    private val topSellingProduct: ProductView
    private val imgBanner: AppCompatImageView
    private val progress: ProgressBar

    init {

        val mainView = inflate(context, R.layout.activity_category, this)

        root = mainView.root_category_activity
        txtTitle = mainView.txt_title_category_activity
        imgBack = mainView.img_back_category_activity
        newProduct = mainView.new_products_category_activity
        topSellingProduct = mainView.top_selling_category_activity
        imgBanner = mainView.img_banner_category_activity
        progress = mainView.progress_category_activity

    }

    fun onClick() {

        imgBack.setOnClickListener { utility.onFinished() }

    }

    fun setDataNewProducts(data: List<DataProduct>) {

        newProduct.initRecycler(data, TypeGetProduct.NEW_PRODUCT)

    }

    fun setDataTopSellingProducts(data: List<DataProduct>) {

        topSellingProduct.initRecycler(data, TypeGetProduct.TOP_SELLING_PRODUCT)
        hideProgress()

    }

    fun setTitle(title: String) {
        txtTitle.text = title
    }

    fun showText(text: String) {

        context.toast(text)

    }

    fun showProgress() {
        progress.visibility = View.VISIBLE
        root.visibility = View.INVISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.INVISIBLE
        root.visibility = View.VISIBLE
    }

}