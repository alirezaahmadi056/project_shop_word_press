package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.AboutMeActivity
import ir.ahmadi.projectshopwordpress.activity.LoginActivity
import ir.ahmadi.projectshopwordpress.activity.QuestionActivity
import ir.ahmadi.projectshopwordpress.etc.SetFragment
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelMainActivity
import ir.ahmadi.projectshopwordpress.net.ApiService
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.dialog_call_me.view.*
import kotlinx.android.synthetic.main.dialog_failure_report.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("ViewConstructor")
class ViewMainActivity(
    contextInstance: Context,
    private val setFragment: SetFragment,
    private val utility: Utility
) : FrameLayout(contextInstance), PopupMenu.OnMenuItemClickListener {

    private val bottomNav: BottomNavigationView
    private val imgMenu: AppCompatImageView

    init {

        val view = inflate(context, R.layout.activity_main, this)

        bottomNav = view.bottom_nav_main
        imgMenu = view.menu_main

    }

    fun setUpView(itemChecked: Int, mainFragment: Fragment) {

        imgMenu.setOnClickListener {

            onCreatePopupMenu()

        }

        bottomNav.menu.getItem(itemChecked).isChecked = true

        addFragment(mainFragment)

    }

    fun bottomNavItemClick(fragments: Map<String, Fragment>) {

        bottomNav.setOnNavigationItemSelectedListener {

            when (it.itemId) {

                R.id.item_menu_account -> {

                    replaceFragment(fragments[ModelMainActivity.KEY_ACCOUNT_FRAGMENT] ?: Fragment())
                    true

                }

                R.id.item_menu_home -> {

                    replaceFragment(fragments[ModelMainActivity.KEY_HOME_FRAGMENT] ?: Fragment())
                    true

                }

                R.id.item_menu_shop -> {

                    replaceFragment(fragments[ModelMainActivity.KEY_SHOP_FRAGMENT] ?: Fragment())
                    true

                }

                else -> false

            }

        }

    }

    private fun addFragment(fragment: Fragment) {

        setFragment.addFragment(fragment)

    }

    private fun replaceFragment(fragment: Fragment) {

        setFragment.replaceFragment(fragment)

    }

    @SuppressLint("InflateParams")
    override fun onMenuItemClick(item: MenuItem?): Boolean {

        return when (item?.itemId) {

            R.id.menu_question -> {

                context.startActivity<QuestionActivity>()
                true

            }

            R.id.menu_about_me -> {

                context.startActivity<AboutMeActivity>()
                true

            }

            R.id.menu_call_me -> {

                val view = LayoutInflater.from(context).inflate(R.layout.dialog_call_me, null)

                AlertDialog.Builder(context)
                    .setView(view)
                    .show()

                view.txt_call_me.setOnClickListener {

                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:09016583758")
                    if (intent.resolveActivity(context.packageManager) != null)
                        context.startActivity(intent)

                }

                true

            }

            R.id.menu_failureReport -> {


                val view =
                    LayoutInflater.from(context).inflate(R.layout.dialog_failure_report, null)

                AlertDialog.Builder(context, R.style.DialogTheme)
                    .setView(view)
                    .setPositiveButton("ارسال") { _, _ ->

                        val subject = view.edt_subject_dialog_failure_report.text.toString()
                        val text = view.edt_text_dialog_failure_report.text.toString()

                        if (subject.isEmpty() || text.isEmpty())
                            context.toast("لطفا فیلد ها را پر کنید")
                        else {

                            ApiService()
                                .getApi()
                                .sendFailureReport(subject, text)
                                .enqueue(object : Callback<Boolean> {

                                    override fun onFailure(call: Call<Boolean>, t: Throwable) {
                                        context.toast("عدم موفقیت در ارسال")
                                    }

                                    override fun onResponse(
                                        call: Call<Boolean>,
                                        response: Response<Boolean>
                                    ) {
                                        context.toast("با موفقیت ارسال شد")
                                    }

                                })

                        }

                    }
                    .setNegativeButton("کنسل") { dialog, _ ->

                        dialog.dismiss()

                    }
                    .setCancelable(false)
                    .show()

                true

            }

            R.id.menu_exit -> {

                val pref = context.getSharedPreferences("pref", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("login", false)
                editor.apply()

                context.startActivity<LoginActivity>()
                utility.onFinished()

                true

            }

            else -> false

        }

    }

    private fun onCreatePopupMenu() {

        val popup = PopupMenu(context, imgMenu)
        popup.menuInflater
            .inflate(R.menu.menu_main, popup.menu)
        popup.setOnMenuItemClickListener(this)
        popup.show()

    }

}