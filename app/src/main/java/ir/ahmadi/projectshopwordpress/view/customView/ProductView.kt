package ir.ahmadi.projectshopwordpress.view.customView

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.ArchiveActivity
import ir.ahmadi.projectshopwordpress.adapter.RecyclerItemProductAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import kotlinx.android.synthetic.main.product_view.view.*
import org.jetbrains.anko.startActivity

class ProductView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    private val txtTitle: AppCompatTextView
    private val txtAll: AppCompatTextView
    private val recycler: RecyclerView

    companion object {

        const val TITLE_KEY = "title"
        const val TYPE_KEY = "type"

    }

    init {

        val mainView = inflate(context, R.layout.product_view, this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProductView)
        val text = typedArray.getString(R.styleable.ProductView_titleText)
        typedArray.recycle()

        txtTitle = mainView.txt_product_title
        txtAll = mainView.txt_product_all
        recycler = mainView.recycler_product

        recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)

        txtTitle.text = text

    }

    fun initRecycler(data: List<DataProduct>, type: TypeGetProduct) {

        txtAll.setOnClickListener {

            context.startActivity<ArchiveActivity>(
                TITLE_KEY to txtTitle.text.toString(),
                TYPE_KEY to type
            )

        }

        recycler.adapter = RecyclerItemProductAdapter(context, data)

    }

}