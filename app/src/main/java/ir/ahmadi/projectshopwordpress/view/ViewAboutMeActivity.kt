package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.WebViewActivity
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelWebViewActivity
import kotlinx.android.synthetic.main.activity_about_me.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

@SuppressLint("ViewConstructor")
class ViewAboutMeActivity(
    contextInstance: Context,
    private val utility: Utility
) :
    FrameLayout(contextInstance) {

    private val imgBack: AppCompatImageView
    private val imgTelegram: AppCompatImageView
    private val imgInstagram: AppCompatImageView
    private val imgSite: AppCompatImageView

    init {

        val mainView = inflate(context, R.layout.activity_about_me, this)

        imgBack = mainView.img_back_about_me_activity
        imgTelegram = mainView.img_telegram_about_me_activity
        imgInstagram = mainView.img_instagram_about_me_activity
        imgSite = mainView.img_site_about_me_activity

    }

    fun onClickHandler(siteAddress: String, telegramUrl: String, instagramUrl: String) {

        imgBack.setOnClickListener { utility.onFinished() }

        imgTelegram.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(telegramUrl))
            intent.setPackage("org.telegram.messenger")

            try {
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity<WebViewActivity>(ModelWebViewActivity.KEY_URL to telegramUrl)
            }

        }

        imgInstagram.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(instagramUrl))
            intent.setPackage("com.instagram.android")

            try {
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity<WebViewActivity>(ModelWebViewActivity.KEY_URL to instagramUrl)
            }

        }

        imgSite.setOnClickListener { context?.startActivity<WebViewActivity>(ModelWebViewActivity.KEY_URL to siteAddress) }

    }

}