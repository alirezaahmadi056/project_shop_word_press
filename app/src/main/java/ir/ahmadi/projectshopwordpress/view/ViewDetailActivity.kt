package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.ScrollView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codesgood.views.JustifiedTextView
import com.google.android.material.snackbar.Snackbar
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.adapter.RecyclerCommentsAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataCategory
import ir.ahmadi.projectshopwordpress.dataClass.DataComments
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.dataClass.MSG
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.utility.PicassoUtility
import ir.ahmadi.projectshopwordpress.view.customView.CustomTextView
import kotlinx.android.synthetic.main.activity_detail.view.*
import org.jetbrains.anko.toast
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("ViewConstructor")
class ViewDetailActivity(
    contextInstance: Context,
    private val utility: Utility
) : FrameLayout(contextInstance), KoinComponent {

    private var test = false
    private val root: RelativeLayout
    private val imgBack: AppCompatImageView
    private val imgFavorite: AppCompatImageView
    private val txtTitle: AppCompatTextView
    private val txtName: AppCompatTextView
    private val txtDescription: JustifiedTextView
    private val txtPrice: AppCompatTextView
    private val txtDiscount: CustomTextView
    private val txtBuy: AppCompatTextView
    private val imgPreview: AppCompatImageView
    private val ratingBar: AppCompatRatingBar
    private val recycler: RecyclerView
    private val progress: ProgressBar
    private val scrollView: ScrollView
    private val picassoUtility: PicassoUtility by inject()

    init {

        val mainView = inflate(context, R.layout.activity_detail, this)

        root = mainView.root_detail_activity
        imgBack = mainView.img_back_detail_activity
        imgFavorite = mainView.img_favorite_detail_activity
        txtTitle = mainView.txt_title_detail_activity
        txtName = mainView.txt_name_detail_activity
        txtDescription = mainView.txt_description_detail_activity
        txtPrice = mainView.txt_price_detail_activity
        txtDiscount = mainView.txt_discount_detail_activity
        txtBuy = mainView.txt_buy_detail_activity
        imgPreview = mainView.img_preview_detail_activity
        ratingBar = mainView.rating_detail_activity
        recycler = mainView.recycler_comments_detail_activity
        progress = mainView.progress_detail_activity
        scrollView = mainView.scrollView_detail_activity

    }

    fun onClickHandler(id: Int, apiService: ApiService) {

        imgBack.setOnClickListener { utility.onFinished() }

        imgFavorite.setOnClickListener {

            test = if (test) {
                imgFavorite.setImageResource(R.drawable.ic_favorite_off)
                false
            } else {
                imgFavorite.setImageResource(R.drawable.ic_favorite_on)
                true
            }

        }

        txtBuy.setOnClickListener {

            apiService
                .getApi()
                .sendProduct(id)
                .enqueue(object : Callback<MSG> {

                    override fun onFailure(call: Call<MSG>, t: Throwable) {
                        context.toast("خطا در ارسال اطلاعات به سرور")
                    }

                    override fun onResponse(call: Call<MSG>, response: Response<MSG>) {

                        val data = response.body()
                        if (data != null)
                            context.toast(data.msg)

                    }

                })

        }

    }

    fun setDataRecycler(data: List<DataComments>) {

        recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)
        recycler.adapter = RecyclerCommentsAdapter(data)

    }

    fun setUpViews(data: DataProduct) {

        picassoUtility.setImage(data.imgAddress, imgPreview)
        txtName.text = data.title
        txtDescription.text = data.description
        ratingBar.rating = data.rate

        if (data.discount == 1) {

            txtDiscount.visibility = View.VISIBLE
            txtDiscount.setCustomText(data.price)
            txtPrice.text = data.priceDiscount

        } else {

            txtDiscount.visibility = View.GONE
            txtPrice.text = data.price

        }

    }

    fun showSnackBar(title: String) {

        progress.visibility = View.INVISIBLE

        val snack = Snackbar.make(
            root,
            title,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setActionTextColor(Color.CYAN)
        snack.setAction("تلاش مجدد") {
            snack.dismiss(); utility.onRefresh(); progress.visibility = View.VISIBLE
        }
        snack.show()

    }

    fun setTitle(title: String) {
        txtTitle.text = title
    }

    fun hideProgress() {

        progress.visibility = View.INVISIBLE
        scrollView.visibility = View.VISIBLE

    }

}