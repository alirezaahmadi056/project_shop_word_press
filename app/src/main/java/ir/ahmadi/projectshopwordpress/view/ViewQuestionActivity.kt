package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.adapter.RecyclerQuestionAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataQuestion
import ir.ahmadi.projectshopwordpress.etc.Utility
import kotlinx.android.synthetic.main.activity_question.view.*
import org.jetbrains.anko.toast

@SuppressLint("ViewConstructor")
class ViewQuestionActivity(
    contextInstance: Context,
    private val utility: Utility
) :
    FrameLayout(contextInstance) {

    private val imgBack: AppCompatImageView
    private val progress: ProgressBar
    private val recycler: RecyclerView

    init {

        val mainView = inflate(context, R.layout.activity_question, this)

        imgBack = mainView.img_back_question_activity
        progress = mainView.progress_activity_question
        recycler = mainView.recycler_activity_question

    }

    fun initRecycler(data: List<DataQuestion>) {

        recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler.adapter = RecyclerQuestionAdapter(data)
        showProgress()

    }

    private fun showProgress() {

        progress.visibility = View.VISIBLE
        recycler.visibility = View.INVISIBLE

    }

    fun hideProgress() {

        progress.visibility = View.INVISIBLE
        recycler.visibility = View.VISIBLE

    }

    fun onClickHandler() {
        imgBack.setOnClickListener { utility.onFinished() }
    }

    fun showToast(text: String) {
        context.toast(text)
    }

}