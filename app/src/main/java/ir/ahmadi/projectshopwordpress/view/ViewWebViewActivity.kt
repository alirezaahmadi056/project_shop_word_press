package ir.ahmadi.projectshopwordpress.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import com.google.android.material.snackbar.Snackbar
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import kotlinx.android.synthetic.main.activity_web_view.view.*

@SuppressLint("ViewConstructor")
class ViewWebViewActivity(
    contextInstance: Context,
    private val utility: Utility
) : FrameLayout(contextInstance) {

    private val root: RelativeLayout
    private val webView: WebView
    private val imgBack: AppCompatImageView

    init {

        val mainView = inflate(context, R.layout.activity_web_view, this)

        root = mainView.root_webView_activity
        webView = mainView.webView_web_view_activity
        imgBack = mainView.img_back_web_view_activity

    }

    fun onClickHandler() {

        imgBack.setOnClickListener { utility.onFinished() }

    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initWebView(url: String) {

        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        webView.settings.textZoom = 100
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)

    }

    fun loadUrl(url: String){
        webView.loadUrl(url)
    }

    fun showSnackBar(title: String) {

        val snack = Snackbar.make(
            root,
            title,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setActionTextColor(Color.CYAN)
        snack.setAction("تلاش مجدد"){ snack.dismiss(); utility.onRefresh() }
        snack.show()

    }

}