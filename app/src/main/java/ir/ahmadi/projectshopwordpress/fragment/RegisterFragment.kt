package ir.ahmadi.projectshopwordpress.fragment


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.MainActivity
import ir.ahmadi.projectshopwordpress.activity.WebViewActivity
import ir.ahmadi.projectshopwordpress.dataClass.DataLogin
import ir.ahmadi.projectshopwordpress.model.ModelWebViewActivity
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.presenter.PresenterRegisterFragment
import kotlinx.android.synthetic.main.fragment_register.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterFragment : Fragment() {

    private val presenter: PresenterRegisterFragment by inject()
    private val apiService: ApiService by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txt_register_fragment.setOnClickListener {

            if (testRight()) {

                progressBar_register_fragment.visibility = View.VISIBLE

                apiService.getApi()
                    .userRegister(
                        edt_email_register_fragment.text.toString(),
                        edt_password_register_fragment.text.toString()
                    )
                    .enqueue(object : Callback<DataLogin> {

                        override fun onFailure(call: Call<DataLogin>, t: Throwable) {
                            toast("خطای اتصال به اینترنت")
                            progressBar_register_fragment.visibility = View.INVISIBLE
                        }

                        override fun onResponse(
                            call: Call<DataLogin>,
                            response: Response<DataLogin>
                        ) {

                            progressBar_register_fragment.visibility = View.INVISIBLE

                            val data = response.body()

                            if (data != null) {

                                if (data.status == "yes") {

                                    val pref =
                                        activity?.getSharedPreferences("pref", Context.MODE_PRIVATE)
                                    val editor = pref?.edit()
                                    editor?.putBoolean("login", true)
                                    editor?.putString(
                                        "name",
                                        edt_name_register_fragment.text.toString()
                                    )
                                    editor?.putString(
                                        "email",
                                        edt_email_register_fragment.text.toString()
                                    )
                                    editor?.apply()

                                    toast(data.msg)

                                    startActivity<MainActivity>()

                                } else
                                    toast(data.msg)

                            }

                        }

                    })

            }

        }

        txt_view_rules_register_fragment.setOnClickListener {
            startActivity<WebViewActivity>(
                ModelWebViewActivity.KEY_URL to "https://www.webroidlearning.ir"
            )
        }

        presenter.onCreate()

    }

    private fun testRight(): Boolean {

        val checkBox = checkBox_register_fragment.isChecked

        return if (edt_email_register_fragment.text.toString()
                .isEmpty() || edt_password_register_fragment.text.toString().isEmpty() ||
            edt_name_register_fragment.text.toString()
                .isEmpty()
        ) {
            toast("لطفا همه فیلد ها را پر کنید")
            false
        } else
            if (checkBox)
                true
            else {
                toast("لطفا با قوانین موافقت کنید")
                false
            }

    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
