package ir.ahmadi.projectshopwordpress.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.model.ModelAccountFragment
import ir.ahmadi.projectshopwordpress.presenter.PresenterAccountFragment
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment() {

    private lateinit var presenter: PresenterAccountFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val model = ModelAccountFragment(activity?.applicationContext)

        presenter = PresenterAccountFragment(this, model)
        presenter.onCreate()

    }

    fun setDataUser(name: String, email: String) {

        txt_name_account_fragment.text = name
        txt_email_account_fragment.text = email

    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
