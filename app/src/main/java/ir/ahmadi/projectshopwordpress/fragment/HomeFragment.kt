package ir.ahmadi.projectshopwordpress.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.WebViewActivity
import ir.ahmadi.projectshopwordpress.adapter.RecyclerCategoryAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataCategory
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.model.ModelWebViewActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterHomeFragment
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject


class HomeFragment : Fragment() {

    private val presenter: PresenterHomeFragment by inject()
    private lateinit var recyclerCategory: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerCategory = recycler_category_home_fragment

        recycler_category_home_fragment.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)

        presenter.onCreate()

        showProgress()

    }

    //regionData

    fun setDataNewProductRecycler(data: List<DataProduct>) {

        new_products_home_fragment.initRecycler(data, TypeGetProduct.NEW_PRODUCT)

    }

    fun setDataDiscountProductRecycler(data: List<DataProduct>) {

        discount_products_home_fragment.initRecycler(data, TypeGetProduct.DISCOUNT_PRODUCT)

    }

    fun setDataTopSellingProductRecycler(data: List<DataProduct>) {

        topSelling_products_home_fragment.initRecycler(data, TypeGetProduct.TOP_SELLING_PRODUCT)
        hideProgress()

    }

    fun setDataRecyclerCategory(data: List<DataCategory>) {

        recyclerCategory.adapter = RecyclerCategoryAdapter(context, data)

    }

    //endregion

    fun onClickHandler() {

        img_banner1_home_fragment.setOnClickListener {

            val url = "https://webroidlearning.ir"
            context?.startActivity<WebViewActivity>(ModelWebViewActivity.KEY_URL to url)

        }

        img_banner2_home_fragment.setOnClickListener {

            val url = "https://webroidlearning.ir"
            context?.startActivity<WebViewActivity>(ModelWebViewActivity.KEY_URL to url)

        }

    }

    private fun showProgress() {
        progress_home_fragment.visibility = View.VISIBLE
        root_home_fragment.visibility = View.INVISIBLE
    }

    private fun hideProgress() {
        progress_home_fragment.visibility = View.INVISIBLE
        root_home_fragment.visibility = View.VISIBLE
    }

    fun showToast(text: String) {

        context?.toast(text)

    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
