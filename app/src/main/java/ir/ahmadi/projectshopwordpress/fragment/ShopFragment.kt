package ir.ahmadi.projectshopwordpress.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.adapter.RecyclerShopAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataLogin
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.presenter.PresenterShopFragment
import kotlinx.android.synthetic.main.fragment_shop.*
import org.jetbrains.anko.support.v4.toast
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShopFragment : Fragment() {

    private val presenter: PresenterShopFragment by inject()
    private lateinit var adapter: RecyclerShopAdapter
    private val apiService: ApiService by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shop, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView_shop_fragment.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        txt_clear_shop_list.setOnClickListener {

            showProgress()

            apiService.getApi()
                .deleteProductInShopProducts()
                .enqueue(object : Callback<DataLogin> {

                    override fun onFailure(call: Call<DataLogin>, t: Throwable) {
                        showToast(t.message.toString())
                    }

                    override fun onResponse(call: Call<DataLogin>, response: Response<DataLogin>) {

                        val data = response.body()
                        if (data != null) {

                            showToast(data.msg)
                            adapter.refresh(listOf())
                            hideProgress()

                        }

                    }

                })

        }

        presenter.onCreate()

    }

    fun showToast(text: String) {
        toast(text)
    }

    fun setUpRecycler(data: List<DataProduct>) {

        adapter = RecyclerShopAdapter(data, context)
        recyclerView_shop_fragment.adapter = adapter

    }

    fun showProgress() {

        progressBar_shop_fragment.visibility = View.VISIBLE
        recyclerView_shop_fragment.visibility = View.INVISIBLE

    }

    fun hideProgress() {

        progressBar_shop_fragment.visibility = View.INVISIBLE
        recyclerView_shop_fragment.visibility = View.VISIBLE

    }

}
