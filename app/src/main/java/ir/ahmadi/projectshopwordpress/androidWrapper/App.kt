package ir.ahmadi.projectshopwordpress.androidWrapper

import android.app.Application
import ir.ahmadi.projectshopwordpress.di.apiModules
import ir.ahmadi.projectshopwordpress.di.fragmentModules
import ir.ahmadi.projectshopwordpress.di.modelModules
import ir.ahmadi.projectshopwordpress.di.presenterModules
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(
            applicationContext,
            listOf(
                fragmentModules,
                modelModules,
                apiModules,
                presenterModules
            )
        )

    }

}