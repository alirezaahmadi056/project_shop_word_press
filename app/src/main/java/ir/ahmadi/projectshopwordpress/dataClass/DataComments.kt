package ir.ahmadi.projectshopwordpress.dataClass

data class DataComments(
    val id: Int,
    val name: String,
    val comment: String
)