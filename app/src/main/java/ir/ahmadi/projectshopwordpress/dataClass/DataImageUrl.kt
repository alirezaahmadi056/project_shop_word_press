package ir.ahmadi.projectshopwordpress.dataClass

data class DataImageUrl(
    val image1: String,
    val image2: String
)