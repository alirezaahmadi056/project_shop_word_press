package ir.ahmadi.projectshopwordpress.dataClass

data class DataQuestion(
    val question: String,
    val answer: String
)