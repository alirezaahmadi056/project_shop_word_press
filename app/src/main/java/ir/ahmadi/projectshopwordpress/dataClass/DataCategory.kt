package ir.ahmadi.projectshopwordpress.dataClass

data class DataCategory(
    val id: Int,
    val title: String
)