package ir.ahmadi.projectshopwordpress.net

interface CountryPresenterListener<M> {

    fun onResponse(data: M)

    fun onFailure(error: String)

}