package ir.ahmadi.projectshopwordpress.net

import ir.ahmadi.projectshopwordpress.dataClass.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

class ApiService {

    interface DataApi {

        @GET("json/categories.json")
        fun getDataCategory(): Call<List<DataCategory>>

        @GET("code/index.php")
        fun getDataNewProducts(): Call<List<DataProduct>>

        @GET("code/discounts.php")
        fun getDataDiscountProducts(): Call<List<DataProduct>>

        @GET("code/counted.php")
        fun getDataTopSellingProducts(): Call<List<DataProduct>>

        @GET("json/data_new_products_by_category_id")
        fun getDataNewProductsByCategoryId(@Query("id") id: Int): Call<List<DataProduct>>

        @GET("json/data_top_selling_products_by_category_id")
        fun getDataTopSellingProductsByCategoryId(@Query("id") id: Int): Call<List<DataProduct>>

        @GET("json/dataQuestion.json")
        fun getDataQuestion(): Call<List<DataQuestion>>

        @GET("json/failureReport.json")
        fun sendFailureReport(
            @Query("subject") subject: String,
            @Query("text") text: String
        ): Call<Boolean>

        @GET("code/show.php")
        fun getProductById(@Query("id") id: Int): Call<DataProduct>

        @GET("code/cart.php")
        fun sendProduct(@Query("id") id: Int): Call<MSG>

        @GET("code/cart_index.php")
        fun getShopProducts(): Call<List<DataProduct>>

        @GET("code/clear_cart.php")
        fun deleteProductInShopProducts(): Call<DataLogin>

        @FormUrlEncoded
        @POST("code/login.php")
        fun userLogin(
            @Field("email") email: String,
            @Field("password") pass: String
        ): Call<DataLogin>

        @FormUrlEncoded
        @POST("code/register.php")
        fun userRegister(
            @Field("email") email: String,
            @Field("password") pass: String
        ): Call<DataLogin>

    }

    fun getApi(): DataApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://webroidlearning.ir/")
            .build()
            .create(DataApi::class.java)

}