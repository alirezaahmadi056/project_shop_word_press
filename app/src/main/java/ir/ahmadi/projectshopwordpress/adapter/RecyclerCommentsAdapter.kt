package ir.ahmadi.projectshopwordpress.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.dataClass.DataComments
import kotlinx.android.synthetic.main.item_recycler_comments.view.*

class RecyclerCommentsAdapter(private val data: List<DataComments>) :
    RecyclerView.Adapter<RecyclerCommentsAdapter.CommentsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CommentsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_recycler_comments,
                    parent,
                    false
                )
        )

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {

        holder.setData(data[position])

    }

    inner class CommentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtName = itemView.txt_name_recycler_comments

        private val txtComment = itemView.txt_comment_recycler_comments
        fun setData(data: DataComments) {

            txtName.text = data.name
            txtComment.text = data.comment

        }

    }

    override fun getItemCount() = data.size

}