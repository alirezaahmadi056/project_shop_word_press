package ir.ahmadi.projectshopwordpress.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.CategoryActivity
import ir.ahmadi.projectshopwordpress.dataClass.DataCategory
import kotlinx.android.synthetic.main.item_recycler_category.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class RecyclerCategoryAdapter(
    private val context: Context?,
    private val data: List<DataCategory>
) : RecyclerView.Adapter<RecyclerCategoryAdapter.CategoryViewHolder>() {

    companion object {

        const val KEY_ID = "id"
        const val KEY_TITLE = "id"

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_recycler_category,
                    parent,
                    false
                )
        )

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {

        holder.setData(data[position])

    }

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtTitle = itemView.txt_title_item_category

        fun setData(data: DataCategory) {

            txtTitle.text = data.title

            txtTitle.setOnClickListener {
                context?.startActivity<CategoryActivity>(
                    KEY_ID to data.id,
                    KEY_TITLE to data.title
                )
            }

        }

    }

    override fun getItemCount() = data.size

}