package ir.ahmadi.projectshopwordpress.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.DetailActivity
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.utility.PicassoUtility
import kotlinx.android.synthetic.main.item_recycler_shop.view.*
import org.jetbrains.anko.startActivity
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class RecyclerShopAdapter(
    private var data: List<DataProduct>,
    private val context: Context? = null
) :
    RecyclerView.Adapter<RecyclerShopAdapter.ShopViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ShopViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_recycler_shop,
                    parent,
                    false
                )
        )

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        holder.setData(data[position])
    }

    inner class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), KoinComponent {

        private val picassoUtility: PicassoUtility by inject()
        private val imgProduct = itemView.img_product_shop_recycler
        private val txtName = itemView.txt_name_shop_recycler
        private val txtPrice = itemView.txt_price_shop_recycler
        private val txtDiscount = itemView.txt_discount_shop_recycler
        private val ratingBar = itemView.ratingBar_shop_recycler

        fun setData(data: DataProduct) {

            picassoUtility.setImage(data.imgAddress, imgProduct)
            txtName.text = data.title

            if (data.discount == 1) {
                txtDiscount.visibility = View.VISIBLE
                txtPrice.text = data.priceDiscount
                txtDiscount.setCustomText(data.price)
            } else {
                txtDiscount.visibility = View.INVISIBLE
                txtPrice.text = data.price
            }

            ratingBar.rating = data.rate

            imgProduct.setOnClickListener {
                context?.startActivity<DetailActivity>(
                    RecyclerItemProductAdapter.KEY_ID to data.id,
                    RecyclerItemProductAdapter.KEY_TITLE to data.title
                )
            }

        }

    }

    fun refresh(data: List<DataProduct>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

}