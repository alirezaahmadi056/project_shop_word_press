package ir.ahmadi.projectshopwordpress.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.activity.DetailActivity
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.utility.PicassoUtility
import kotlinx.android.synthetic.main.item_recycler_product_view.view.*
import org.jetbrains.anko.startActivity
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class RecyclerItemProductAdapter(
    private val context: Context,
    private val data: List<DataProduct>
) :
    RecyclerView.Adapter<RecyclerItemProductAdapter.ItemProductViewHolder>() {

    companion object {

        const val KEY_ID = "id"
        const val KEY_TITLE = "title"

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemProductViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recycler_product_view,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ItemProductViewHolder, position: Int) {

        holder.setData(data[position])

    }

    inner class ItemProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        KoinComponent {

        private val picasso: PicassoUtility by inject()
        private val rootView = itemView.constraint_recycler_product_view
        private val img = itemView.img_recycler_product_view
        private val title = itemView.txt_title_recycler_product_view
        private val price = itemView.txt_price_recycler_product_view
        private val discount = itemView.customTextView_discount

        fun setData(data: DataProduct) {

            picasso.setImage(data.imgAddress, img)
            title.text = data.title

            if (data.discount == 1) {

                discount.visibility = View.VISIBLE
                discount.setCustomText(data.price)
                price.text = data.priceDiscount

            } else {

                discount.visibility = View.INVISIBLE
                price.text = data.price

            }

            rootView.setOnClickListener {
                context.startActivity<DetailActivity>(
                    KEY_ID to data.id,
                    KEY_TITLE to data.title
                )
            }

        }

    }

    override fun getItemCount() = data.size

}