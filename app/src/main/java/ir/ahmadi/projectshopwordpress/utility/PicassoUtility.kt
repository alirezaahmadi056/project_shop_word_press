package ir.ahmadi.projectshopwordpress.utility

import android.widget.ImageView
import com.squareup.picasso.Picasso
import ir.ahmadi.projectshopwordpress.R
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class PicassoUtility : KoinComponent {

    private val picasso: Picasso by inject()

    fun setImage(address: String, img: ImageView) {

        picasso
            .load(address)
            .placeholder(R.drawable.place_holder)
            .error(R.drawable.error)
            .fit()
            .into(img)

    }

}