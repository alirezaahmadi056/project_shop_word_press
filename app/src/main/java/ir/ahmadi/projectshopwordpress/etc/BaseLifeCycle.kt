package ir.ahmadi.projectshopwordpress.etc

interface BaseLifeCycle {

    fun onCreate()

    fun onDestroy()

    fun onResume(){}

    fun onStop(){}

    fun onRefresh(){}

}