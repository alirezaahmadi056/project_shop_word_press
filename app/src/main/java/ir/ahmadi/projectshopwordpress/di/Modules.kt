package ir.ahmadi.projectshopwordpress.di

import com.squareup.picasso.Picasso
import ir.ahmadi.projectshopwordpress.fragment.*
import ir.ahmadi.projectshopwordpress.model.*
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.presenter.PresenterHomeFragment
import ir.ahmadi.projectshopwordpress.presenter.PresenterLoginFragment
import ir.ahmadi.projectshopwordpress.presenter.PresenterRegisterFragment
import ir.ahmadi.projectshopwordpress.presenter.PresenterShopFragment
import ir.ahmadi.projectshopwordpress.utility.PicassoUtility
import org.koin.dsl.module.module

val fragmentModules = module {

    single { HomeFragment() }
    single { ShopFragment() }
    single { AccountFragment() }
    factory { LoginFragment() }
    factory { RegisterFragment() }

}

val modelModules = module {

    single { ModelMainActivity() }
    single { ModelHomeFragment() }
    factory { ModelQuestionActivity() }
    factory { ModelAboutMeActivity() }
    factory { ModelLoginFragment() }
    factory { ModelRegisterFragment() }
    single { ModelShopFragment() }

}

val apiModules = module {

    single { Picasso.with(get()) }
    single { PicassoUtility() }
    single { ApiService() }

}

val presenterModules = module {

    single { PresenterHomeFragment(get() as HomeFragment, get() as ModelHomeFragment) }
    factory { PresenterLoginFragment(get() as LoginFragment, get() as ModelLoginFragment) }
    factory { PresenterRegisterFragment(get() as RegisterFragment, get() as ModelRegisterFragment) }
    single { PresenterShopFragment(get() as ShopFragment, get() as ModelShopFragment) }

}