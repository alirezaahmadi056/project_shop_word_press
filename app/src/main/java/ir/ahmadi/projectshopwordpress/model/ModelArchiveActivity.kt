package ir.ahmadi.projectshopwordpress.model

import android.app.Activity
import android.util.Log
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.test.DataTest
import ir.ahmadi.projectshopwordpress.view.customView.ProductView
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelArchiveActivity(private val activity: Activity) : KoinComponent {

    private val apiService: ApiService by inject()

    fun getTitleAsIntent() = activity.intent.getStringExtra(ProductView.TITLE_KEY) ?: ""

    fun getDataNewProducts(mListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataNewProducts()
            .enqueue(object : Callback<List<DataProduct>>{

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getDataDiscountProducts(mListener: CountryPresenterListener<List<DataProduct>>){

        apiService.getApi()
            .getDataDiscountProducts()
            .enqueue(object :Callback<List<DataProduct>>{
                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getDataTopSellingProducts(mListener: CountryPresenterListener<List<DataProduct>>){

        apiService.getApi()
            .getDataTopSellingProducts()
            .enqueue(object :Callback<List<DataProduct>>{

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getTypeProduct() =
        activity.intent.getSerializableExtra(ProductView.TYPE_KEY) as TypeGetProduct

}