package ir.ahmadi.projectshopwordpress.model

import android.content.Context

class ModelAccountFragment(private val context: Context? = null) {

    fun getName() = context?.getSharedPreferences("pref", Context.MODE_PRIVATE)
        ?.getString("name", "Default Name") ?: ""

    fun getEmail() = context?.getSharedPreferences("pref", Context.MODE_PRIVATE)
        ?.getString("email", "Default Email") ?: ""

}