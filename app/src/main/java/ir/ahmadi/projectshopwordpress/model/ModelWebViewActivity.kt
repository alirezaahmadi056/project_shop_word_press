package ir.ahmadi.projectshopwordpress.model

import android.app.Activity
import ir.ahmadi.projectshopwordpress.utility.NetworkChecker

class ModelWebViewActivity(private val context: Activity) {

    companion object {

        const val KEY_URL = "url"

    }

    fun getUrlAsIntent() = context.intent.getStringExtra(KEY_URL) ?: "www.daneshjooyar.com"

    fun getNetworkState() = NetworkChecker(context).networkState()

    fun getTitleDialog() = "Not Connection"

}