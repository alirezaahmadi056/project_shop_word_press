package ir.ahmadi.projectshopwordpress.model

class ModelAboutMeActivity {

    fun getSiteAddress() = "http://www.daneshjooyar.ir"

    fun getTelegramUrl() = "https://t.me/daneshjooyar"

    fun getInstagramUrl() = "http://instagram.com/daneshjooyar"

}