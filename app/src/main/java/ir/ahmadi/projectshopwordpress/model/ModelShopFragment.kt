package ir.ahmadi.projectshopwordpress.model

import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelShopFragment : KoinComponent {

    private val apiService: ApiService by inject()

    fun getShopProducts(countryPresenterListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getShopProducts()
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {
                    countryPresenterListener.onFailure("سبد خرید شما خالی میباشد")
                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        countryPresenterListener.onResponse(data)
                    else
                        countryPresenterListener.onFailure("سبد خرید شما خالی میباشد")

                }

            })

    }

}