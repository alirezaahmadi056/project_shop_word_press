package ir.ahmadi.projectshopwordpress.model

import android.app.Activity
import ir.ahmadi.projectshopwordpress.adapter.RecyclerCategoryAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelCategoryActivity(private val activity: Activity) : KoinComponent {

    private val apiService: ApiService by inject()

    private fun getIdAsIntent() = activity.intent.getIntExtra(RecyclerCategoryAdapter.KEY_ID, 0)

    fun getTitleAsIntent() =
        activity.intent.getStringExtra(RecyclerCategoryAdapter.KEY_TITLE) ?: ""

    fun getDataNewProductsById(countryPresenterListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataNewProductsByCategoryId(getIdAsIntent())
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    countryPresenterListener.onFailure(t.message.toString())

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        countryPresenterListener.onResponse(data)

                }

            })

    }

    fun getDataTopSellingProductsById(countryPresenterListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataTopSellingProductsByCategoryId(getIdAsIntent())
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    countryPresenterListener.onFailure(t.message.toString())

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        countryPresenterListener.onResponse(data)

                }

            })

    }

}