package ir.ahmadi.projectshopwordpress.model

import android.app.Activity
import ir.ahmadi.projectshopwordpress.adapter.RecyclerItemProductAdapter
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.test.DataTest
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelDetailActivity(private val context: Activity) : KoinComponent {

    private val apiService: ApiService by inject()

    fun getIdAsIntent() = context.intent.getIntExtra(RecyclerItemProductAdapter.KEY_ID, 0)

    fun getTitleAsIntent() =
        context.intent.getStringExtra(RecyclerItemProductAdapter.KEY_TITLE) ?: ""

    fun getDataRecycler() = DataTest.getDataComments()

    fun getObjectApiService() = apiService

    fun getProductById(countryPresenterListener: CountryPresenterListener<DataProduct>) {

        apiService
            .getApi()
            .getProductById(getIdAsIntent())
            .enqueue(object : Callback<DataProduct> {

                override fun onFailure(call: Call<DataProduct>, t: Throwable) {

                    countryPresenterListener.onFailure("خط در دریافت طلاعات ازسرور")

                }

                override fun onResponse(call: Call<DataProduct>, response: Response<DataProduct>) {

                    val data = response.body()
                    if (data != null)
                        countryPresenterListener.onResponse(data)
                    else
                        countryPresenterListener.onFailure("عدم وجود اطلاعات")

                }

            })

    }

}