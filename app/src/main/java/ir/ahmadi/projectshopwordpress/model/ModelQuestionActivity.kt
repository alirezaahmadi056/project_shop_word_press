package ir.ahmadi.projectshopwordpress.model

import ir.ahmadi.projectshopwordpress.dataClass.DataQuestion
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelQuestionActivity : KoinComponent {

    private val apiService: ApiService by inject()

    fun getDataQuestion(countryPresenterListener: CountryPresenterListener<List<DataQuestion>>) {

        apiService.getApi()
            .getDataQuestion()
            .enqueue(object : Callback<List<DataQuestion>> {

                override fun onFailure(call: Call<List<DataQuestion>>, t: Throwable) {

                    countryPresenterListener.onFailure("خطا در ارتباط با سرور")

                }

                override fun onResponse(
                    call: Call<List<DataQuestion>>,
                    response: Response<List<DataQuestion>>
                ) {

                    val data = response.body()
                    if (data != null)
                        countryPresenterListener.onResponse(data)
                    else
                        countryPresenterListener.onFailure("خطا در دریافت اطاعات از سرور")

                }

            })

    }

}