package ir.ahmadi.projectshopwordpress.model

import ir.ahmadi.projectshopwordpress.fragment.AccountFragment
import ir.ahmadi.projectshopwordpress.fragment.HomeFragment
import ir.ahmadi.projectshopwordpress.fragment.ShopFragment
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class ModelMainActivity : KoinComponent {

    private val homeFragment: HomeFragment by inject()
    private val accountFragment: AccountFragment by inject()
    private val shopFragment: ShopFragment by inject()

    companion object{

        const val KEY_HOME_FRAGMENT = "homeFragment"
        const val KEY_ACCOUNT_FRAGMENT = "accountFragment"
        const val KEY_SHOP_FRAGMENT = "shopFragment"

    }

    fun getItemChecked() = 1

    fun getMainFragment() = homeFragment

    fun getAllFragments() = mapOf(
        KEY_HOME_FRAGMENT to homeFragment,
        KEY_ACCOUNT_FRAGMENT to accountFragment,
        KEY_SHOP_FRAGMENT to shopFragment
    )

}