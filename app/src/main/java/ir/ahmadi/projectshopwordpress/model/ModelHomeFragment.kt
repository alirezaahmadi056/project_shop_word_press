package ir.ahmadi.projectshopwordpress.model

import android.util.Log
import ir.ahmadi.projectshopwordpress.dataClass.DataCategory
import ir.ahmadi.projectshopwordpress.dataClass.DataImageUrl
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.net.ApiService
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.test.DataTest
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelHomeFragment : KoinComponent {

    private val apiService: ApiService by inject()

    fun getDataNewProducts(mListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataNewProducts()
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()
                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getDataDiscountProducts(mListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataDiscountProducts()
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()

                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getDataTopSellingProducts(mListener: CountryPresenterListener<List<DataProduct>>) {

        apiService.getApi()
            .getDataTopSellingProducts()
            .enqueue(object : Callback<List<DataProduct>> {

                override fun onFailure(call: Call<List<DataProduct>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataProduct>>,
                    response: Response<List<DataProduct>>
                ) {

                    val data = response.body()

                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

    fun getDataCategory(mListener: CountryPresenterListener<List<DataCategory>>) {

        apiService.getApi()
            .getDataCategory()
            .enqueue(object : Callback<List<DataCategory>> {

                override fun onFailure(call: Call<List<DataCategory>>, t: Throwable) {

                    Log.e("ERROR_FAILURE", "error in get data : ${t.message}")
                    mListener.onFailure("خطا در دریافت اطلاعات از سرور")

                }

                override fun onResponse(
                    call: Call<List<DataCategory>>,
                    response: Response<List<DataCategory>>
                ) {

                    val data = response.body()

                    if (data != null)
                        mListener.onResponse(data)
                    else
                        Log.e("ERROR_NULL_DATA", "null_data")

                }

            })

    }

}