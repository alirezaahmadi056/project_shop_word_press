package ir.ahmadi.projectshopwordpress.model

import ir.ahmadi.projectshopwordpress.net.ApiService
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class ModelLoginFragment : KoinComponent {

    private val apiService: ApiService by inject()

    fun getObjectApiService() = apiService

}