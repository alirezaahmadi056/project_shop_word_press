package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.enumeration.TypeGetProduct
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelArchiveActivity
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.view.ViewArchiveActivity

class PresenterArchiveActivity(
    private val view: ViewArchiveActivity,
    private val model: ModelArchiveActivity
) : BaseLifeCycle {

    override fun onCreate() {

        onClickListener()
        setDataInRecycler()
        setTitleText()

    }

    private fun setDataInRecycler() {

        when (model.getTypeProduct()) {

            TypeGetProduct.NEW_PRODUCT -> {

                model.getDataNewProducts(object : CountryPresenterListener<List<DataProduct>> {

                    override fun onResponse(data: List<DataProduct>) {
                        view.setDataInRecyclers(data)
                    }

                    override fun onFailure(error: String) {
                        view.showToast(error)
                    }

                })

            }

            TypeGetProduct.DISCOUNT_PRODUCT -> {

                model.getDataDiscountProducts(object : CountryPresenterListener<List<DataProduct>>{

                    override fun onResponse(data: List<DataProduct>) {
                        view.setDataInRecyclers(data)
                    }

                    override fun onFailure(error: String) {
                        view.showToast(error)
                    }

                })

            }

            TypeGetProduct.TOP_SELLING_PRODUCT -> {

                model.getDataTopSellingProducts(object : CountryPresenterListener<List<DataProduct>>{

                    override fun onResponse(data: List<DataProduct>) {
                        view.setDataInRecyclers(data)
                    }

                    override fun onFailure(error: String) {
                        view.showToast(error)
                    }

                })

            }

        }

    }

    private fun setTitleText() {

        view.setTitleText(model.getTitleAsIntent())

    }

    private fun onClickListener() {

        view.onClickHandler()

    }

    override fun onDestroy() {}

}