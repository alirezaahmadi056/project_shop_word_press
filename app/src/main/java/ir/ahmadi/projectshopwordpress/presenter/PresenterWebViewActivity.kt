package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelWebViewActivity
import ir.ahmadi.projectshopwordpress.view.ViewWebViewActivity

class PresenterWebViewActivity(
    private val view: ViewWebViewActivity,
    private val model: ModelWebViewActivity
) : BaseLifeCycle {

    override fun onCreate() {

        init()
        onClick()

    }

    override fun onRefresh() {

        if (model.getNetworkState())
            view.loadUrl(model.getUrlAsIntent())
        else
            view.showSnackBar(model.getTitleDialog())

    }

    private fun init() {

        if (model.getNetworkState())
            view.initWebView(model.getUrlAsIntent())
        else
            view.showSnackBar(model.getTitleDialog())

    }

    private fun onClick() {
        view.onClickHandler()
    }

    override fun onDestroy() {}

}