package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.fragment.LoginFragment
import ir.ahmadi.projectshopwordpress.model.ModelLoginFragment

class PresenterLoginFragment(
    private val view: LoginFragment,
    private val model: ModelLoginFragment
) : BaseLifeCycle {

    override fun onCreate() {

    }

    override fun onDestroy() {}

}