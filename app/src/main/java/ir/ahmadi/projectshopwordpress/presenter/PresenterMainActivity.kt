package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelMainActivity
import ir.ahmadi.projectshopwordpress.view.ViewMainActivity

class PresenterMainActivity(
    private val view: ViewMainActivity,
    private val model: ModelMainActivity
) : BaseLifeCycle {

    override fun onCreate() {

        setUpView()
        setBottomNavItemClick()

    }

    private fun setUpView() {

        view.setUpView(model.getItemChecked(), model.getMainFragment())

    }

    private fun setBottomNavItemClick() {

        view.bottomNavItemClick(model.getAllFragments())

    }

    override fun onDestroy() {}

}