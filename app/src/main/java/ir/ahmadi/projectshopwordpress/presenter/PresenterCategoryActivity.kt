package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelCategoryActivity
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.view.ViewCategoryActivity

class PresenterCategoryActivity(
    private val view: ViewCategoryActivity,
    private val model: ModelCategoryActivity
) : BaseLifeCycle {

    override fun onCreate() {

        showProgress()
        setTitle()
        onClickHandler()
        setDataRecyclers()

    }

    private fun setDataRecyclers() {

        model.getDataNewProductsById(object : CountryPresenterListener<List<DataProduct>> {

            override fun onResponse(data: List<DataProduct>) {
                view.setDataNewProducts(data)
            }

            override fun onFailure(error: String) {
                view.showText(error)
            }

        })

        model.getDataTopSellingProductsById(object : CountryPresenterListener<List<DataProduct>> {

            override fun onResponse(data: List<DataProduct>) {
                view.setDataTopSellingProducts(data)
            }

            override fun onFailure(error: String) {
                view.showText(error)
            }

        })

    }

    private fun showProgress() {
        view.showProgress()
    }

    private fun onClickHandler() {
        view.onClick()
    }

    private fun setTitle() {
        view.setTitle(model.getTitleAsIntent())
    }

    override fun onDestroy() {}

}