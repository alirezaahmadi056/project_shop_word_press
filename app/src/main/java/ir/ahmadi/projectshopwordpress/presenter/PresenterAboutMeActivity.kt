package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelAboutMeActivity
import ir.ahmadi.projectshopwordpress.view.ViewAboutMeActivity

class PresenterAboutMeActivity(
    private val view: ViewAboutMeActivity,
    private val model: ModelAboutMeActivity
) : BaseLifeCycle {

    override fun onCreate() {

        onClick()

    }

    private fun onClick() {
        view.onClickHandler(model.getSiteAddress(), model.getTelegramUrl(), model.getInstagramUrl())
    }

    override fun onDestroy() {}

}