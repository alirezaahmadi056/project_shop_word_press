package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelLoginActivity
import ir.ahmadi.projectshopwordpress.view.ViewLoginActivity

class PresenterLoginActivity(
    private val view: ViewLoginActivity,
    private val model: ModelLoginActivity
) : BaseLifeCycle {

    override fun onCreate() {

        checkNet()
        setUpTabLayout()

    }

    private fun checkNet() {
        view.checkInternet(model.checkNetInfo())
    }

    private fun setUpTabLayout() {
        view.setUpTabLayout(model.getAdapter(), model.getObjectLogin(), model.getObjectRegister())
    }

    override fun onDestroy() {}

}