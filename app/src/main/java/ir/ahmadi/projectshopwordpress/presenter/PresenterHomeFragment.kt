package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataCategory
import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.fragment.HomeFragment
import ir.ahmadi.projectshopwordpress.model.ModelHomeFragment
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener

class PresenterHomeFragment(
    private val view: HomeFragment,
    private val model: ModelHomeFragment
) : BaseLifeCycle {

    override fun onCreate() {

        setUpGetDataCategory()
        setUpGetDataRecyclers()
        setOnClick()

    }

    private fun setUpGetDataCategory() {

        model.getDataCategory(object : CountryPresenterListener<List<DataCategory>> {

            override fun onResponse(data: List<DataCategory>) {
                view.setDataRecyclerCategory(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }

        })

    }

    private fun setUpGetDataRecyclers() {

        model.getDataNewProducts(object : CountryPresenterListener<List<DataProduct>> {

            override fun onResponse(data: List<DataProduct>) {
                view.setDataNewProductRecycler(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }

        })

        model.getDataDiscountProducts(object : CountryPresenterListener<List<DataProduct>> {

            override fun onResponse(data: List<DataProduct>) {
                view.setDataDiscountProductRecycler(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }

        })

        model.getDataTopSellingProducts(object : CountryPresenterListener<List<DataProduct>> {

            override fun onResponse(data: List<DataProduct>) {
                view.setDataTopSellingProductRecycler(data)
            }

            override fun onFailure(error: String) {
                view.showToast(error)
            }

        })

    }

    private fun setOnClick() {
        view.onClickHandler()
    }

    override fun onDestroy() {}

}