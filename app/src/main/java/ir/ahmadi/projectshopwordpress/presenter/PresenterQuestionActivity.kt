package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataQuestion
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelQuestionActivity
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.view.ViewQuestionActivity

class PresenterQuestionActivity(
    private val view: ViewQuestionActivity,
    private val model: ModelQuestionActivity
) : BaseLifeCycle {

    override fun onCreate() {

        onClick()
        getDataAsApi()

    }

    private fun onClick() {
        view.onClickHandler()
    }

    private fun getDataAsApi() {

        model.getDataQuestion(object : CountryPresenterListener<List<DataQuestion>> {

            override fun onResponse(data: List<DataQuestion>) {

                view.initRecycler(data)
                view.hideProgress()

            }

            override fun onFailure(error: String) {

                view.showToast(error)
                view.hideProgress()

            }

        })

    }

    override fun onDestroy() {}

}