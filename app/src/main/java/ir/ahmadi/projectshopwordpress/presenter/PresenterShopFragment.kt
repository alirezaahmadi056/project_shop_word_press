package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.fragment.ShopFragment
import ir.ahmadi.projectshopwordpress.model.ModelShopFragment
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener

class PresenterShopFragment(
    private val view: ShopFragment,
    private val model: ModelShopFragment
) : BaseLifeCycle, CountryPresenterListener<List<DataProduct>> {

    override fun onCreate() {

        getShopProducts()

    }

    override fun onDestroy() {}

    private fun getShopProducts() {
        view.showProgress()
        model.getShopProducts(this)
    }

    override fun onResponse(data: List<DataProduct>) {
        view.setUpRecycler(data)
        view.hideProgress()
    }

    override fun onFailure(error: String) {
        view.showToast(error)
        view.hideProgress()
    }

}