package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.fragment.RegisterFragment
import ir.ahmadi.projectshopwordpress.model.ModelRegisterFragment

class PresenterRegisterFragment(
    private val view: RegisterFragment,
    private val model: ModelRegisterFragment
) : BaseLifeCycle {

    override fun onCreate() {

    }

    override fun onDestroy() {}

}