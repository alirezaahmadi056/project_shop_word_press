package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.dataClass.DataProduct
import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.model.ModelDetailActivity
import ir.ahmadi.projectshopwordpress.net.CountryPresenterListener
import ir.ahmadi.projectshopwordpress.view.ViewDetailActivity

class PresenterDetailActivity(
    private val view: ViewDetailActivity,
    private val model: ModelDetailActivity
) : BaseLifeCycle, CountryPresenterListener<DataProduct> {

    override fun onCreate() {

        setTitle()
        onClick()
        setDataRecycler()
        setDataProduct()

    }

    override fun onRefresh() {
        setDataRecycler()
        setDataProduct()
    }

    private fun onClick() {
        view.onClickHandler(model.getIdAsIntent(), model.getObjectApiService())
    }

    private fun setDataRecycler() {
        view.setDataRecycler(model.getDataRecycler())
    }

    private fun setDataProduct() {
        model.getProductById(this)
    }

    private fun setTitle() {
        view.setTitle(model.getTitleAsIntent())
    }

    override fun onResponse(data: DataProduct) {
        view.setUpViews(data)
        view.hideProgress()
    }

    override fun onFailure(error: String) {
        view.showSnackBar(error)
    }

    override fun onDestroy() {

    }

}