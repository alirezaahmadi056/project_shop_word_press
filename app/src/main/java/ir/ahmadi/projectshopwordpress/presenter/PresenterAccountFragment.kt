package ir.ahmadi.projectshopwordpress.presenter

import ir.ahmadi.projectshopwordpress.etc.BaseLifeCycle
import ir.ahmadi.projectshopwordpress.fragment.AccountFragment
import ir.ahmadi.projectshopwordpress.model.ModelAccountFragment

class PresenterAccountFragment(
    private val view: AccountFragment,
    private val model: ModelAccountFragment
) : BaseLifeCycle {

    override fun onCreate() {

        setDataUser()

    }

    private fun setDataUser() {
        view.setDataUser(model.getName(), model.getEmail())
    }

    override fun onDestroy() {}

}