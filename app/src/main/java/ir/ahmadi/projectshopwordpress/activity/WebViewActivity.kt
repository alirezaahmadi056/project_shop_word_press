package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelWebViewActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterWebViewActivity
import ir.ahmadi.projectshopwordpress.view.ViewWebViewActivity

class WebViewActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterWebViewActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewWebViewActivity(this, this)
        val model = ModelWebViewActivity(this)

        setContentView(view)

        presenter = PresenterWebViewActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onRefresh() {
        presenter.onRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
