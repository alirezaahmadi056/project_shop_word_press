package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.adapter.RecyclerCategoryAdapter
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelCategoryActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterCategoryActivity
import ir.ahmadi.projectshopwordpress.view.ViewCategoryActivity

class CategoryActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterCategoryActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewCategoryActivity(this, this)
        val model = ModelCategoryActivity(this)

        setContentView(view)

        presenter = PresenterCategoryActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
