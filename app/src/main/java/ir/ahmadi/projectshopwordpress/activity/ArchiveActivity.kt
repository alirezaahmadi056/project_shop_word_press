package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelArchiveActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterArchiveActivity
import ir.ahmadi.projectshopwordpress.view.ViewArchiveActivity

class ArchiveActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterArchiveActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewArchiveActivity(this, this)

        setContentView(view)

        val model = ModelArchiveActivity(this)

        presenter = PresenterArchiveActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
