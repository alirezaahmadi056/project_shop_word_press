package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelDetailActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterDetailActivity
import ir.ahmadi.projectshopwordpress.view.ViewDetailActivity

class DetailActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterDetailActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewDetailActivity(this, this)
        val model = ModelDetailActivity(this)

        setContentView(view)

        presenter = PresenterDetailActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onRefresh() {
        presenter.onRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
