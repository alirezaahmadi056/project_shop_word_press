package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelAboutMeActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterAboutMeActivity
import ir.ahmadi.projectshopwordpress.view.ViewAboutMeActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject

class AboutMeActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterAboutMeActivity
    private val model: ModelAboutMeActivity by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewAboutMeActivity(this, this)

        setContentView(view)

        presenter = PresenterAboutMeActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
