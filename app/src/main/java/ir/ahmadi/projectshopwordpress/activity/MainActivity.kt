package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.SetFragment
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelMainActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterMainActivity
import ir.ahmadi.projectshopwordpress.view.ViewMainActivity
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(), SetFragment, Utility {

    private val model: ModelMainActivity by inject()
    private lateinit var presenter: PresenterMainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewMainActivity(this, this, this)

        setContentView(view)

        presenter = PresenterMainActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun addFragment(fragment: Fragment) {

        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_frame, fragment)
            .commit()

    }

    override fun replaceFragment(fragment: Fragment) {

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_frame, fragment)
            .commit()

    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

}
