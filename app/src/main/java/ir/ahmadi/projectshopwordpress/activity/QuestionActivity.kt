package ir.ahmadi.projectshopwordpress.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelQuestionActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterQuestionActivity
import ir.ahmadi.projectshopwordpress.view.ViewQuestionActivity
import org.koin.android.ext.android.inject

class QuestionActivity : AppCompatActivity(), Utility {

    private val model: ModelQuestionActivity by inject()
    private lateinit var presenter: PresenterQuestionActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = ViewQuestionActivity(this, this)

        setContentView(view)

        presenter = PresenterQuestionActivity(view, model)
        presenter.onCreate()

    }

    override fun onFinished() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
