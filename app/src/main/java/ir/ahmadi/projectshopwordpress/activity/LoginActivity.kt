package ir.ahmadi.projectshopwordpress.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ir.ahmadi.projectshopwordpress.R
import ir.ahmadi.projectshopwordpress.etc.Utility
import ir.ahmadi.projectshopwordpress.model.ModelLoginActivity
import ir.ahmadi.projectshopwordpress.presenter.PresenterLoginActivity
import ir.ahmadi.projectshopwordpress.view.ViewLoginActivity
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity(), Utility {

    private lateinit var presenter: PresenterLoginActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkLogin()

        val view = ViewLoginActivity(this, this)
        val model = ModelLoginActivity(this)

        setContentView(view)

        presenter = PresenterLoginActivity(view, model)
        presenter.onCreate()

    }

    override fun onRefresh() {
        presenter.onCreate()
    }

    private fun checkLogin() {

        val pref = getSharedPreferences("pref", Context.MODE_PRIVATE)

        if (pref.getBoolean("login", false))
            startActivity<MainActivity>()

    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()

    }

}
